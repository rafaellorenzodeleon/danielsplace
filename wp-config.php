<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'daniels_place_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=X:FYO8ju]eILt-nGCJZ-v*cF2:z=^3rqY6r<JGd|! zVsH(ytRuCexgMzFbPK;a');
define('SECURE_AUTH_KEY',  'zXb~@k?SLf4iT71_o60/qU7#9vm1s_-4s(.Tvu71+$8%4Sd81{[!^%Rg8u!q<$<M');
define('LOGGED_IN_KEY',    '/fCN+6|bta#g*-uv9.LCm15DC~F6{hO0EJ.wkc ;i{.e9GIV6x/`~)>@ZzRaG!4t');
define('NONCE_KEY',        '-(AP#i`?=_C.M[5#yaF-Fg$.KvS^di.glrjP35`S*v9~)`h-:u/cL6G%|hm;*hK}');
define('AUTH_SALT',        'krnb9&4gVwwglhU<mi5Z5FpbqdLY@`aA.3IEO|ENS/w5x[VO4xklEmz8xdT?BL3L');
define('SECURE_AUTH_SALT', 'b^1!%Yq:P#V(3p6&l]K~@X}F:h}`u3Y2k^k9t]prPmC7voh`}|P7&0`m=Mql}^hN');
define('LOGGED_IN_SALT',   'M<>? I#t0)bg2n7WGk?26jy>k8$GNjr]fR*?L;P|JlPj0l3pDnQ|fV}`oL7]?eGr');
define('NONCE_SALT',       'HcY-JHqjIVPAP{[Oz]BMia(&H> ZrU;*9 2HM8_)4^ZgHLj{@|D#FyQ@bx**g.l]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


<?php
/**
 * Template Name: FAQ
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div class="main-wrapper">
		<h1 class="content-header">Frequently Asked Questions</h1>

		<div class="faq-wrap">
			<ul>
				<li>
					<p class="faq-h">Is there a standard check-in/check-out time for guests at Daniel’s Place?</p>
					<p>There is no standard check-in/check-out time at Daniel’s Place. However, guests will need to set what time they will be arriving and leaving the resort upon reservation to ensure that the resort is ready and that the timeslot will not conflict with another group. Preferred timeslot is subject to availability of the resort. We suggest that you book and confirm as early as possible to ensure you can check-in at your preferred time.</p>
				</li>
				<li>
					<p class="faq-h">Can guests stay for more than 24 hours at Daniel’s Place?</p>
					<p>Yes, we offer both long-term and short-term rates. Please contact us for preferred rates if you wish to stay longer.</p>
				</li>
				<li>
					<p class="faq-h">How do we make a reservation?</p>
					<p>Upon confirmation that the date and timeslot are available, guests are requested to make a minimal deposit through our BDO or BPI accounts to ensure that your slot is reserved under your name. The reservation fee is deductible to the total rental rate agreed upon. Full payment is requested upon checking in at the resort.  Please notify us once you have made a deposit to confirm your reservation.</p>
				</li>
				<li>
					<p class="faq-h">Is our reservation deposit refundable?</p>
					<p>Although the reservation deposit is non-refundable, you can cancel your booking and rebook for another date and time if notice is given a week before the original date of your reservation.</p>
				</li>
				<li>
					<p class="faq-h">How deep is the adult and kiddie pool?</p>
					<p>The adult pool is 4-5 foot-deep while the kiddie pool is 3 foot-deep.</p>
				</li>
				<li>
					<p class="faq-h">How many beds are in the cottage?</p>
					<p>There are two double-sized beds on the second floor and a cushioned day bed on the ground floor of the kubo. There are also extra folding mattresses available upon request at no extra cost.</p>
				</li>
				<li>
					<p class="faq-h">Do we need to bring cooking utensils at Daniel’s Place?</p>
					<p>Basic cooking utensils are provided in the resort for your convenience. Guests will just need to bring their own eating utensils (i.e. plates, spoon &amp; fork, cups and drinking glasses).</p>
				</li>
				<li>
					<p class="faq-h">Are bed linens and towels provided at the resort?</p>
					<p>All beds have mattresses with mattress covers and pillows with fresh set of pillowcases. Guests will need to bring their own blankets as well as towels for hygienic purposes.</p>
				</li>
				<li>
					<p class="faq-h">Is there a water dispenser in the resort?</p>
					<p>The drinking water refill being sold near the resort comes in a container with dispenser. Please coordinate with the resort staff if you wish to order in advance.</p>
				</li>
			</ul>
		</div>

		<div class="clear spacer50"></div>
	</div>
</div>

<?php
get_footer();

<?php
/**
 * Template Name: About Us
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div class="main-wrapper">
		<h1 class="content-header">A new place for peace and relaxation</h1>

		<div class="content-wrap">
			<p>
				<img class="about-img" src="<?php echo get_template_directory_uri(); ?>/img/about/Kubo-1.jpg">
				Daniel's Place Private Resort is a 426-square meter property right at the heart of Pansol in Calamba, Laguna where hot spring pools with therapeutic properties is the main attraction. Its proximity to the metropolis - about 54 kilometers south of Manila, makes it an ideal spot for those who wish to quickly escape from the fast-paced life in the city. Since this private resort is situated right along the rim of the legendary Mount Makiling, the resort is a haven for nature-lovers. The old town of Calamba also banks on its rich history in providing guests staying in the resort so much to explore in and around its neighboring towns.<br><br>
 
				Daniel's Place can accommodate small to medium-sized groups with all its structures facing the hot spring pool. The design concept is anchored on the Filipinos’ culture when it comes to close family ties and love for food and wholesome entertainment. The two-storey structure inspired by the bahay kubo houses the social areas of the resort where families and friends can spend quality time together. A compact and functional kitchen opens to the dining area and salas on the ground floor. The second level has an open layout that gives the guests the flexibility to use it for afternoon siestas or accommodate overnight guests and extended family members. The high ceiling and wide window panels on all sides fill the room with a lot of natural breeze and awash it with an abundance of natural lighting – a few of the rare commodities when you are living in a crowded city. The casita by the swimming pool has two bedrooms providing more privacy for guests who wish to rest while the rest of the group are still gathered in the kubo.  Its Filipino tropical theme imbibes a relaxing and refreshing feel with each corner of the resort having a view of or access to the outdoors.<br><br>
				 
				With its strategic location and comfortable amenities, there is just more than enough to fill the senses at Daniel's Place Private Resort.
 
			</p>
		</div>

		<div class="clear spacer50"></div>
	</div>
</div>

<?php
get_footer();
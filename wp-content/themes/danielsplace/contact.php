
<?php
/**
 * Template Name: Contact Us
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div class="main-wrapper">
		<h1 class="content-header">For your inquiries</h1>
		<p class="content-sub-text">
			<b>Contact us!</b><br>
			<b>02-7269536 | 02-7269518</b><br>
			<b>0917-4225239 | 0922-6047414 | 0927-8418481</b><br><br>

			Email us at:<br>
			<b>lulettem@yahoo.com</b><br>
		</p>


		<div class="clear rate-spacer"></div>


		<div class="content-wrap c-form">
			<p class="c-head">Message Us!</p>
			<?php echo do_shortcode('[contact-form-7 id="47" title="Contact"]'); ?>
		</div>
		
		<div class="clear spacer50"></div>
	</div>
</div>

<?php
get_footer();
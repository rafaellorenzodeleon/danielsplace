			<!-- footer -->
			<footer id="footer" class="footer" role="contentinfo">

				<nav class="nav" role="navigation">
					<?php html5blank_nav(); ?>
				</nav>

				<p>www.danielsplaceresort.com</p>

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>
	</body>
</html>

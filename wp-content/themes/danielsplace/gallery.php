
<?php
/**
 * Template Name: Gallery
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div class="main-wrapper">
		<h1 class="content-header">Take a tour around the resort</h1>

		<div class="content-wrap">
			<p>Take a peek at what you'll see in <b>Daniel's Place Private Resort.</b><br><br>

			We guarantee that our guests are well-accomodated and receiving our full range of service. Feel free to comment on each photo, or <a class="gallery-contact" href="/contact-us/">send us a message</a> if you have any questions.</p>
			
			<div class="gallery-images">
				<p>Around the Resort</p>
				<div class="g-images">
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/Daniel%27s%20Place.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Daniel%27s%20Place.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/Adult%20%26%20kiddie%20pool.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Adult%20%26%20kiddie%20pool.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/Balcony.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Balcony.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/Dining%20area.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Dining%20area.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/Kubo.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Kubo.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/Kubo%20foyer.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Kubo%20foyer.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2757.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2757.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2747.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2747.JPG" alt="" />
					</a>
				</div>
				<div class="clear"></div>
			</div>

			<div class="clear rate-spacer"></div>

			<div class="gallery-images">
				<p>Rooms and Interior</p>
				<div class="g-images">
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/BR1.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/BR1.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/T%26B1.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/T%26B1.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/The%20loft.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/The%20loft.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/Kubo%20ground%20floor.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Kubo%20ground%20floor.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2730.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2730.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2732.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2732.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2733.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2733.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2744.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2744.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2723.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2723.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2720.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_2720.JPG" alt="" />
					</a>

					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/new/kitchen-dining/1.jpg" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/new/kitchen-dining/1.jpg" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/new/kitchen-dining/1.jpg" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/new/kitchen-dining/2.jpg" alt="" />
					</a>

					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110751.jpg" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110751.jpg" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110634.jpg" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110634.jpg" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110642.jpg" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110642.jpg" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110719.jpg" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110719.jpg" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110722.jpg" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/new/kubo/20180516_110722.jpg" alt="" />
					</a>
					


				</div>
				<div class="clear"></div>
			</div>

			<div class="clear rate-spacer"></div>

			<div class="gallery-images">
				<p>At Night</p>
				<div class="g-images">
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3551.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3551.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3069.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3069.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3065.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3065.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3531.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3531.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3570.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3570.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3573.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3573.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3566_0.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3566_0.JPG" alt="" />
					</a>
					<a class="col-md-3" href="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3529.JPG" data-fancybox data-caption="">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3529.JPG" alt="" />
					</a>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="clear spacer50"></div>
	</div>
</div>

<?php
get_footer();

<?php
/**
 * Template Name: Home Page
 */
get_header(); ?>
<div id="home-slider">
	<div class="slide s1" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/homepage/facade_image_01.jpg')"></div>
	<div class="slide s2" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/homepage/facade_image_02.jpg')"></div>
</div>
<div id="main-content" class="main-content">
	<div class="home-main-wrapper">
		<div class="clear spacer30"></div>
		
		<!-- INTRO -->
		<div class="content-wrap">
			<div class="col-md-6">
				<h1 class="content-header">Be closer to nature, Be closer to home</h1>
				<p>
					Daniel's Place Private Resort is a 426-square meter property right at the heart of Pansol in Calamba, Laguna where hot spring pools with therapeutic properties is the main attraction. Its proximity to the metropolis - about 54 kilometers south of Manila, makes it an ideal spot for those who wish to quickly escape from the fast-paced life in the city.
				</p>
				<a class="page-btn" href="<?php echo get_home_url(); ?>/about-us/">Learn More</a>
			</div>
			<div class="col-md-6">
				<img src="<?php echo get_template_directory_uri(); ?>/img/homepage/facade_mini.png">
			</div>
			<div class="clear"></div>
			<div class="clear spacer30"></div>
		</div>
		
		

		<!-- END -->
		<div class="section-wrap">
			<div class="clear spacer30"></div>
			<div class="content-wrap">
				<div class="col-md-6">
					<div id="home-gallery">
						<div class="hg-slide" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/gallery/about-us-image.jpg')"></div>
						<div class="hg-slide" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/gallery/DP_rates_01.jpg')"></div>
						<div class="hg-slide" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/gallery/panel_rates.jpg')"></div>
						<div class="hg-slide" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/gallery/IMG_3551.jpg')"></div>
						<div class="hg-slide" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/gallery/Kubo-1.jpg')"></div>
					</div>
				</div>
				<div class="col-md-6">
					<h1 class="content-header">Take a tour around the resort</h1>
					<p>Daniel's Place offers you reasonably low prices. Choose the right package for you, whether for a family outing or a whole barkada excursion.</p>
					<a class="page-btn" href="<?php echo get_home_url(); ?>/gallery/">View Gallery</a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div class="clear spacer30"></div>
		</div>

		<!-- END -->
		<div class="content-wrap">
			<div class="clear spacer30"></div>

			<div class="col-md-8">
				<h1 class="content-header">Affordable Rates and Packages</h1>
				<p>See what Daniel's Place has in store for you! Get a quick tour around the private resort and check out the activities and amenities available.</p>
				<a class="page-btn" href="<?php echo get_home_url(); ?>/rates-packages/">View Packages</a>
				<div class="clear spacer20"></div>


				<div>
					<h1 class="content-header">Contact Us</h1>
					<p>
						Just send us your questions or concerns by starting a new case and we will give you the help you need.

						<ul class="col-md-3 home-list">
							<li>0917-422-5239</li>
							<li>0922-604-7414</li>
							<li>0927-841-8481</li>
						</ul>
						
						<ul class="col-md-3 home-list">
							<li>0917-422-5239</li>
							<li>0922-604-7414</li>
							<li>0927-841-8481</li>
						</ul>
						<div class="clear"></div>
					</p>
					<a class="page-btn" href="<?php echo get_home_url(); ?>/contact-us/">Contact Us</a>

				</div>
				<div class="clear spacer20"></div>
			</div>

			<div class="col-md-4">
				<a href="<?php echo get_home_url(); ?>/reservation/"><img class="home-tile-img" src="<?php echo get_template_directory_uri(); ?>/img/homepage/dp_reserve_button.png"></a>
				<div class="clear spacer20"></div>
				<iframe src="//www.facebook.com/plugins/likebox.php?href=http://www.facebook.com/danielsplaceresort&amp;width=290&amp;colorscheme=light&amp;show_faces=true&amp;bordercolor&amp;stream=false&amp;header=true&amp;height=280&amp;show_border=false&amp;force_wall=false" scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: 100%; height: 280px;" allowtransparency="true">
				</iframe>
			</div>

			<div class="clear spacer30"></div>
			<div class="clear"></div>
		</div>
		

		<!-- MAP -->
		<div>
			<iframe id="gmap" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7736.630869255973!2d121.186409!3d14.176294!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33bd614e016e1bbd%3A0xb22a492caea5e967!2sDaniel&#39;s+Place+Resort!5e0!3m2!1sen!2sph!4v1525783372995" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			<div class="clear"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#home-slider').slick({
		  dots: false,
		  infinite: true,
		  speed: 1000,
		  fade: true,
		  arrows:false,
		  autoplaySpeed: 5000,
		  autoplay: true
		});

		$('#home-gallery').slick({
		  dots: false,
		  infinite: true,
		  speed: 1000,
		  arrows:false,
		  autoplaySpeed: 3000,
		  autoplay: true
		});
	})
</script>

<?php
get_footer();
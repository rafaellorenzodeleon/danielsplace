
<?php
/**
 * Template Name: Location
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div class="main-wrapper">
		<h1 class="content-header">Location &amp; Map</h1>

		<div class="content-wrap">
			<div class="g-map">
				<iframe id="gmap" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7736.630869255973!2d121.186409!3d14.176294!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33bd614e016e1bbd%3A0xb22a492caea5e967!2sDaniel&#39;s+Place+Resort!5e0!3m2!1sen!2sph!4v1525783372995" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				<p><a href="https://goo.gl/maps/H7G4wwtnVfJ2" target="_blank">Click here</a> to view on Google Maps.</p>
			</div>

			<div class="clear rate-spacer"></div>

			<div class="img-map">
				<p>
					<span>Download this printable map</span>
					Download this map and print for your reference when you go to Daniel's Place Private Resort.
					Right click on the link and select "Save As" to download to your computer.

					<a href="<?php echo get_template_directory_uri(); ?>/img/location/Daniels_Place_map_print.pdf" class="download-map" target="_blank">Download Map</a>
				</p>
				<img src="<?php echo get_template_directory_uri(); ?>/img/location/location_map.jpg">
			</div>

			<div class="clear rate-spacer"></div>

			<div class="directions">
				
				<p>Directions to Daniel's Place:</p>
				<ul>
					<li>Coming from South Luzon Expressway, take the Calamba exit and stay on the National Highway going towards Los Baños. </li>
					<li>Turn right on the street between Andok’s, Sol Y Viento archway and Wonderspring Resort (E. Kaimo Street). </li>
					<li>Turn left at the end and go straight ahead until you reach the gate of Pansol Hot Spring Village.</li>
					<li>Do not enter the gate but turn right on the narrow road (Cuevas Street).</li>
					<li>Turn right on the second street after JC Manzano Street cornering Palmridge Resort, same street going to JP Resort and Casa Real.</li>
					<li>We are the third gate from the corner....a brown gate with the sign DANIEL'S PLACE. </li>
				</ul>

			</div>
			
		</div>

		<div class="clear spacer50"></div>
	</div>
</div>

<?php
get_footer();

<?php
/**
 * Template Name: Rates and Packages
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div class="main-wrapper">
		<h1 class="content-header">Choose your package</h1>

		<div class="rate-wrap">
			<img src="<?php echo get_template_directory_uri(); ?>/img/rates-packages/packhead_daniels.png">
			<div class="rate-details">
				<p><b>Daniel's Package</b> allows access to the entire resort. Good for up to 25 persons.</p>
				
				<div class="col-md-6">
					<ul>
						<li>2 air conditioned rooms</li>
						<li>1 loft-type fan room</li>
						<li>3 toilets and baths</li>
						<li>living area</li>
						<li>dining area</li>
						<li>kitchen area with ref/freezer and 2 burner gas stove (free gas for cooking)</li>
						<li>barbeque grilling area</li>
						<li>2 outdoor showers</li>
						<li>adult and kiddie hot spring swimming pools</li>
						<li>unlimited use of videoke</li>
					</ul>
				</div>
				<div class="col-md-6 rate-image">
					<img src="<?php echo get_template_directory_uri(); ?>/img/rates-packages/DP_rates_01.jpg">
				</div>
				
				<div class="clear"></div>

				<p class="details-head">Rates</p>

				<div class="col-md-6 rate-table">
					<table>
						<thead><th colspan="2">Off-Peak Season</th></thead>
						<tbody>
							<tr>
								<td>24-Hour Rate</td>
								<td>P 8,500 ~ 9,500</td>
							</tr>
							<tr>
								<td>12-Hour Rate</td>
								<td>P 6,500 ~ 7,500</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-6 rate-table">
					<table>
						<thead><th colspan="2">Peak Season</th></thead>
						<tbody>
							<tr>
								<td>24-Hour Rate</td>
								<td>P 13,000 ~ 14,000</td>
							</tr>
							<tr>
								<td>12-Hour Rate</td>
								<td>P 9,000 ~ 10,000</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="rate-spacer clear"></div>

		<div class="rate-wrap">
			<img src="<?php echo get_template_directory_uri(); ?>/img/rates-packages/packhead_kubo.png">
			<div class="rate-details">
				<p><b>Kubo Package</b> allows access to the swimming pool &amp; main house. Good for up to 10 persons.</p>
				
				<div class="col-md-6">
					<ul>
						<li>1 loft-type fan room</li>
						<li>1 toilet &amp; bath</li>
						<li>living area</li>
						<li>dining area</li>
						<li>kitchen area with ref/freezer and 2 burner gas stove (free gas for cooking)</li>
						<li>barbeque grilling area</li>
						<li>2 outdoor showers</li>
						<li>adult and kiddie hot spring swimming pools</li>
						<li>unlimited use of videoke</li>
					</ul>
				</div>
				<div class="col-md-6 rate-image">
					<img src="<?php echo get_template_directory_uri(); ?>/img/rates-packages/kubo_rates_01.jpg">
				</div>
				
				<div class="clear"></div>

				<p class="details-head">Rates</p>

				<div class="col-md-6 rate-table">
					<table>
						<thead><th colspan="2">Off-Peak Season</th></thead>
						<tbody>
							<tr>
								<td>24-Hour Rate</td>
								<td>P 6,500 ~ 7,500</td>
							</tr>
							<tr>
								<td>12-Hour Rate</td>
								<td>P 5,000</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-6 rate-table">
					<table>
						<thead><th colspan="2">Peak Season</th></thead>
						<tbody>
							<tr>
								<td>24-Hour Rate</td>
								<td>P 9,000 ~ 10,000</td>
							</tr>
							<tr>
								<td>12-Hour Rate</td>
								<td>P 7,000 ~ 8,000</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="rate-spacer clear"></div>

		<div class="rate-wrap">
			<img src="<?php echo get_template_directory_uri(); ?>/img/rates-packages/packhead_cottage.png">
			<div class="rate-details">
				<p><b>Cottage Package</b> allows access to the swimming pool and pool house. Good for up to 10 persons.</p>
				
				<div class="col-md-6">
					<ul>
						<li>1 or 2 air conditioned rooms</li>
						<li>2 toilets and baths</li>
						<li>dining area</li>
						<li>kitchen area with ref/freezer and 2 burner gas stove (free gas for cooking)</li>
						<li>barbeque grilling area</li>
						<li>2 outdoor showers</li>
						<li>adult and kiddie hot spring swimming pools</li>
						<li>unlimited use of videoke</li>
					</ul>
				</div>
				<div class="col-md-6 rate-image">
					<img src="<?php echo get_template_directory_uri(); ?>/img/rates-packages/cott_rates_01.jpg">
				</div>
				
				<div class="clear"></div>

				<p class="details-head">Rates</p>

				<div class="col-md-6 rate-table">
					<table>
						<thead><th colspan="2">Off-Peak Season</th></thead>
						<tbody>
							<tr>
								<td>24-Hour Rate</td>
								<td>P 7,500 ~ 8,500</td>
							</tr>
							<tr>
								<td>12-Hour Rate</td>
								<td>P 5,500 ~ 6,500</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-6 rate-table">
					<table>
						<thead><th colspan="2">Peak Season</th></thead>
						<tbody>
							<tr>
								<td>24-Hour Rate</td>
								<td>P 10,000 ~ 11,000</td>
							</tr>
							<tr>
								<td>12-Hour Rate</td>
								<td>P 8,000 ~ 9,000</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="rate-spacer clear"></div>

		<div class="rate-wrap">
			<div class="rate-details">
				<p><b>Add-ons:</b></p>
				
				<div class="col-md-12 add-ons">
					<ul>
						<li><b>P100</b>/person in excess of allowed no. of persons</li>
						<li><b>P500</b>/hour time extension</li>
						<li><b>P200 ~ P500</b> electric consumption for major electrical appliances such as microwave oven and mobile sound system</li>
					</ul>
				</div>
				<div class="col-md-6 rate-image">
				</div>
				
				<div class="clear"></div>

			</div>
		</div>


		<div class="clear spacer50"></div>
	</div>
</div>

<?php
get_footer();
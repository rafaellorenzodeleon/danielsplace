
<?php
/**
 * Template Name: Reservation
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div class="main-wrapper">
		<h1 class="content-header">Book your stay now!</h1>
		<p class="content-sub-text">Your booking will be confirmed within the next 12 hours. If you have received no reply from us, please contact us immediately on the numbers posted in the <a href="/contact-us/" target="_blank">Contact Us</a> page. Thank you!</p>

		<div class="content-wrap c-form">
			<?php echo do_shortcode('[contact-form-7 id="46" title="Reservation"]'); ?>
		</div>
		
		<div class="clear spacer50"></div>
	</div>
</div>

<?php
get_footer();